## ApiBoot是什么？

`ApiBoot`是接口服务的落地解决方案，依赖于`SpringBoot`，提供了一系列开箱即用的组件，通过封装来简化主流第三方框架的集成，从而提高开发者`开发效率`、`学习成本`、`降低入门门槛`，真正的实现`开箱即用`。

## 官方文档 & 源码
- 官方文档：[http://apiboot.minbox.io](http://apiboot.minbox.io)
- 码云：[https://gitee.com/minbox-projects/api-boot](https://gitee.com/minbox-projects/api-boot)
- GitHub：[https://github.com/hengboy/api-boot](https://github.com/hengboy/api-boot)

## 请给我支持

`ApiBoot`框架目前是由博客作者编写开源框架，请给我一定的支持，让我坚持去下，为开源做贡献。

- 请关注作者的公众号`“程序员恒宇少年”`，二维码在页面底部，关注后回复"资料"获取专属电子小册
- 请将该页面分享给更多需要它的技术学习爱好者
- 请给`ApiBoot`源码仓库点个`Star`，`Watching`后可以收到每次发版的通知。
  - [Gitee](https://gitee.com/minbox-projects/api-boot)
  - [GitHub](https://github.com/hengboy/api-boot)

## 福利来袭

![](https://blog.yuqiyu.com/images/%E7%AD%BE%E5%88%B0%E7%A6%8F%E5%88%A9.png)

> 自律改变人生 - [点击参与签到赠书计划](https://blog.yuqiyu.com/welfare/)

## 任务调度组件
- apiboot-quartz-job-types：<a href="https://blog.yuqiyu.com/apiboot-quartz-job-types.html" target="_blank">分布式调度框架Quartz衍生出的三种任务类型，你用过几个？</a>
- apiboot-quartz-integrated-away：<a href="https://blog.yuqiyu.com/apiboot-quartz-integrated-away.html" target="_blank">这种方式整合Quartz你见过吗？</a>

## 文档组件

- apiboot-swagger-describe-the-interface：<a href="https://blog.yuqiyu.com/apiboot-swagger-describe-the-interface.html" target="_blank">使用Swagger2作为文档来描述你的接口信息</a>
- apiboot-swagger-integrated-oauth：<a href="https://blog.yuqiyu.com/apiboot-swagger-integrated-oauth.html" target="_blank">Swagger2怎么整合OAuth2来在线调试接口？</a>

## 安全组件

> 格式：源码目录：对应文章
- apiboot-oauth-multiple-client-config：<a href="https://blog.yuqiyu.com/apiboot-oauth-multiple-client-config.html" target="_blank">OAuth2在内存、Redis、JDBC方式下的多客户端配置</a>
- apiboot-security-oauth-first-application：<a href="https://blog.yuqiyu.com/apiboot-security-oauth-zero-code-integration.html" target="_blank">实现零代码整合Spring Security & OAuth2</a>
- apiboot-security-customize-select-user：<a href="https://blog.yuqiyu.com/apiboot-security-customize-select-user.html" target="_blank">ApiBoot零代码整合Spring Security的JDBC方式获取AccessToken</a>
- apiboot-security-oauth-custom-certification-user：<a href="https://blog.yuqiyu.com/apiboot-security-oauth-custom-certification-user.html" target="_blank">见过这么简单的方式整合SpringSecurity & OAuth2查询用户吗？</a>
- apiboot-define-oauth-grant-type：<a href="https://blog.yuqiyu.com/apiboot-security-open-paths-without-intercept.html" target="_blank">原来SpringSecurity整合OAuth2后开放权限拦截路径还能这么玩？</a>
- apiboot-security-open-paths-without-intercept：<a href="https://blog.yuqiyu.com/apiboot-security-open-paths-without-intercept.html" target="_blank">原来SpringSecurity整合OAuth2后开放权限拦截路径还能这么玩？</a>
- apiboot-security-oauth-use-jwt：<a href="https://blog.yuqiyu.com/apiboot-security-oauth-use-jwt.html" target="_blank">还不会使用JWT格式化OAuth2令牌吗？</a>
- apiboot-oauth-set-token-expire-time：<a href="https://blog.yuqiyu.com/apiboot-oauth-set-token-expire-time.html" target="_blank">来看看OAuth2怎么设置AccessToken有效期时间时长</a>
- apiboot-oauth-use-redis-storage：<a href="https://blog.yuqiyu.com/apiboot-oauth-use-redis-storage.html" target="_blank">OAuth2使用Redis来存储客户端信息以及AccessToken</a>

## 分布式日志组件

- apiboot-logging-use-global-log：<a href="https://blog.yuqiyu.com/apiboot-logging-use-global-log.html" target="_blank">《ApiBoot新特性》GlobalLog全局日志的使用详解</a>
- apiboot-unified-manage-request-logs：<a href="https://blog.yuqiyu.com/apiboot-unified-manage-request-logs.html" target="_blank">使用ApiBoot Logging进行统一管理请求日志</a>
- apiboot-report-logs-by-logging-to-admin：<a href="https://blog.yuqiyu.com/apiboot-report-logs-by-logging-to-admin.html" target="_blank">将ApiBoot Logging采集的日志上报到Admin</a>
- apiboot-custom-logging-traceid：<a href="https://blog.yuqiyu.com/apiboot-custom-logging-traceid.html" target="_blank">自定义ApiBoot Logging链路以及单元ID</a>
- modify-apiboot-logging-collection-prefix：<a href="https://blog.yuqiyu.com/modify-apiboot-logging-collection-prefix.html" target="_blank">修改ApiBoot Logging日志采集的前缀</a>
- apiboot-logging-integrates-spring-security：<a href="https://blog.yuqiyu.com/apiboot-logging-integrates-spring-security.html" target="_blank">ApiBoot Logging整合Spring Security安全上报日志</a>
- apiboot-logging-integrates-eureka-report-logs：<a href="https://blog.yuqiyu.com/apiboot-logging-integrates-eureka-report-logs.html" target="_blank">ApiBoot Logging整合SpringCloud Eureka负载均衡上报日志</a>
- apiboot-logging-using-openfeign-transparent-traceid：<a href="https://blog.yuqiyu.com/apiboot-logging-using-openfeign-transparent-traceid.html" target="_blank">ApiBoot Logging使用SpringCloud Openfeign透传链路信息</a>
- apiboot-logging-using-resttemplate-transparent-traceid：<a href="https://blog.yuqiyu.com/apiboot-logging-using-resttemplate-transparent-traceid.html" target="_blank">ApiBoot Logging使用RestTemplate透传链路信息</a>
- apiboot-logging-admin-visual-interface-management-log：<a href="https://blog.yuqiyu.com/apiboot-logging-admin-visual-interface-management-log.html" target="_blank">ApiBoot Logging Admin可视化界面管理日志</a>


## 其他组件

更多组件的使用文章正在火热连载更新...

## 作者公众号

  <img src="https://blog.yuqiyu.com/images/mp.jpg" width="150"/>