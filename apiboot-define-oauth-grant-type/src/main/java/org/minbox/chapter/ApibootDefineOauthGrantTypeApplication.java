package org.minbox.chapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApibootDefineOauthGrantTypeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApibootDefineOauthGrantTypeApplication.class, args);
    }

}
