package org.minbox.chapter.service;

import org.minbox.chapter.entity.PhoneCode;
import org.minbox.chapter.mapper.PhoneCodeEnhanceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 验证码业务逻辑实现
 *
 * @author 恒宇少年
 */
@Service
public class PhoneCodeService {
    /**
     * 手机号验证码数据接口
     */
    @Autowired
    private PhoneCodeEnhanceMapper mapper;

    /**
     * 查询手机号验证码
     *
     * @param phone {@link PhoneCode#getPhone()}
     * @param code  {@link PhoneCode#getCode()}
     * @return
     */
    public PhoneCode findPhoneCode(String phone, String code) {
        return mapper.findByPhoneAndCode(phone, code);
    }
}
