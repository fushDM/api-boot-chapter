package org.minbox.chapter.apiboot.custom.logging.id;

import org.minbox.framework.logging.client.MinBoxLoggingException;
import org.minbox.framework.logging.client.tracer.LoggingTraceGenerator;

import java.util.UUID;

/**
 * 自定义traceId生成策略
 *
 * @author 恒宇少年
 */
public class CustomTraceIdGenerator implements LoggingTraceGenerator {
    /**
     * 链路编号前缀
     */
    private static final String TRACE_ID_PREFIX = "local";

    @Override
    public String createTraceId() throws MinBoxLoggingException {
        return TRACE_ID_PREFIX + UUID.randomUUID().toString().hashCode();
    }
}
