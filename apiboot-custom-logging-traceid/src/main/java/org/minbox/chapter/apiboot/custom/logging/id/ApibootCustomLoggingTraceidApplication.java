package org.minbox.chapter.apiboot.custom.logging.id;

import org.minbox.framework.logging.spring.context.annotation.client.EnableLoggingClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableLoggingClient
@RestController
public class ApibootCustomLoggingTraceidApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApibootCustomLoggingTraceidApplication.class, args);
    }

    @GetMapping(value = "/index")
    public String index() {
        return "this is index.";
    }

}
