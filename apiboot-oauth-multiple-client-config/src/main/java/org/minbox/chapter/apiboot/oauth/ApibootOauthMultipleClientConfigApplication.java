package org.minbox.chapter.apiboot.oauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApibootOauthMultipleClientConfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApibootOauthMultipleClientConfigApplication.class, args);
	}

}
