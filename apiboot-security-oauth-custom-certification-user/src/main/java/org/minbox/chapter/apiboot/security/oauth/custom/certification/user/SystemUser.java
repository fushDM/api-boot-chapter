package org.minbox.chapter.apiboot.security.oauth.custom.certification.user;

import com.gitee.hengboy.mybatis.enhance.common.annotation.Column;
import com.gitee.hengboy.mybatis.enhance.common.annotation.Id;
import com.gitee.hengboy.mybatis.enhance.common.annotation.Table;
import com.gitee.hengboy.mybatis.enhance.common.enums.KeyGeneratorTypeEnum;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * 系统用户基本信息
 *
 * @author 恒宇少年
 */
@Data
@Table(name = "system_user")
public class SystemUser implements UserDetails {
    /**
     * 用户编号
     */
    @Id(generatorType = KeyGeneratorTypeEnum.UUID)
    @Column(name = "su_id")
    private String userId;
    /**
     * 登录名
     */
    @Column(name = "su_login_name")
    private String loginName;
    /**
     * 昵称
     */
    @Column(name = "su_nick_name")
    private String nickName;
    /**
     * 密码
     */
    @Column(name = "su_password")
    private String password;
    /**
     * 创建时间
     */
    @Column(name = "su_create_time")
    private String createTime;
    /**
     * 用户状态
     * 1：正常，0：已冻结，-1：已删除
     */
    @Column(name = "su_status")
    private Integer status;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public String getUsername() {
        return this.loginName;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    /**
     * UserDetails提供的方法，用户是否未过期
     * 可根据自己用户数据表内的字段进行扩展，这里为了演示配置为true
     *
     * @return
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * UserDetails提供的方法，用户是否未锁定
     * 可根据自己用户数据表内的字段进行扩展，这里为了演示配置为true
     *
     * @return
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * UserDetails提供的方法，凭证是否未过期
     * 可根据自己用户数据表内的字段进行扩展，这里为了演示配置为true
     *
     * @return
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * UserDetails提供的方法，是否启用
     *
     * @return
     */
    @Override
    public boolean isEnabled() {
        return this.status == 1;
    }
}
