package org.minbox.chapter.apiboot.security.oauth.custom.certification.user;

import com.gitee.hengboy.mybatis.enhance.mapper.EnhanceMapper;
import org.apache.ibatis.annotations.Param;

/**
 * ApiBoot Enhance提供的增强Mapper
 * 自动被扫描并且注册到IOC
 *
 * @author 恒宇少年
 * @see org.minbox.framework.api.boot.autoconfigure.enhance.ApiBootMyBatisEnhanceAutoConfiguration
 */
public interface SystemUserEnhanceMapper extends EnhanceMapper<SystemUser, String> {
    /**
     * 根据用户登录名查询用户信息
     *
     * @param loginName {@link SystemUser#getLoginName()}
     * @return {@link SystemUser}
     */
    SystemUser findByLoginName(@Param("loginName") String loginName);
}
