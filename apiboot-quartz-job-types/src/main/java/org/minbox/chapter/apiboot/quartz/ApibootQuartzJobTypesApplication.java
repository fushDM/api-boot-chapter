package org.minbox.chapter.apiboot.quartz;

import org.minbox.framework.api.boot.plugin.quartz.ApiBootQuartzService;
import org.minbox.framework.api.boot.plugin.quartz.wrapper.support.ApiBootCronJobWrapper;
import org.minbox.framework.api.boot.plugin.quartz.wrapper.support.ApiBootLoopJobWrapper;
import org.minbox.framework.api.boot.plugin.quartz.wrapper.support.ApiBootOnceJobWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ApiBoot Quartz 三种任务类型示例
 */
@SpringBootApplication
public class ApibootQuartzJobTypesApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ApibootQuartzJobTypesApplication.class, args);
    }

    /**
     * ApiBoot Quartz内置接口
     *
     * @see org.minbox.framework.api.boot.plugin.quartz.support.ApiBootQuartzServiceDefaultSupport
     */
    @Autowired
    private ApiBootQuartzService quartzService;

    @Override
    public void run(String... args) throws Exception {
        // 一次性任务
        quartzService.newJob(ApiBootOnceJobWrapper.Context().jobClass(DemoJob.class).wrapper());

        // 循环执行任务，每隔2000毫秒执行一次，循环5次，一共执行6次
        quartzService.newJob(ApiBootLoopJobWrapper.Context().jobClass(DemoJob.class).loopIntervalTime(2000).repeatTimes(5).wrapper());

        // Cron表达式任务，间隔1秒执行一次
        quartzService.newJob(ApiBootCronJobWrapper.Context().jobClass(DemoJob.class).cron("0/1 * * * * ?").wrapper());
    }
}
