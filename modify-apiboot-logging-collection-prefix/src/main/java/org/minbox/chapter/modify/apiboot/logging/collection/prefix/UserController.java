package org.minbox.chapter.modify.apiboot.logging.collection.prefix;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 恒宇少年
 */
@RestController
@RequestMapping(value = "/user")
public class UserController {
    /**
     * 测试日志拦截路径接口
     *
     * @param name
     * @return
     */
    @GetMapping
    public String welcome(@RequestParam("name") String name) {
        return "hello, " + name;
    }

    /**
     * 用户信息
     * /user/info
     *
     * @return
     */
    @GetMapping(value = "/info")
    public String info() {
        return "this is user info";
    }
}
