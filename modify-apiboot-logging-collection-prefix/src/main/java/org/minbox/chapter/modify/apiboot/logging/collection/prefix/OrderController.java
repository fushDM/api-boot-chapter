package org.minbox.chapter.modify.apiboot.logging.collection.prefix;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @author 恒宇少年
 */
@RestController
@RequestMapping(value = "/order")
public class OrderController {

    @PostMapping
    public String submit() {
        return "订单：" + UUID.randomUUID().toString() + "，提交成功.";
    }
}
