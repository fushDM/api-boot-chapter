package org.minbox.chapter.apiboot.security;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 示例：控制器
 *
 * @author 恒宇少年
 */
@RestController
@RequestMapping(value = "/index")
public class IndexController {
    /**
     * 示例：首页地址
     * /index
     *
     * @return
     */
    @GetMapping
    public String index() {
        return "this is index page.";
    }

    /**
     * 示例：首页地址子页面
     * /index/sub
     *
     * @return
     */
    @GetMapping(value = "/sub")
    public String indexSub() {
        return "this is sub index page.";
    }
}
