package org.minbox.chapter.apiboot.oauth.token.expire.time;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApibootOauthSetTokenExpireTimeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApibootOauthSetTokenExpireTimeApplication.class, args);
    }

}
