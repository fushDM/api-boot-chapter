package org.minbox.chapter.apiboot.oauth;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试Api控制器
 *
 * @author 恒宇少年
 */
@RestController
@RequestMapping(value = "/api")
public class ApiController {
    /**
     * 测试请求，需携带令牌访问
     *
     * @return
     */
    @GetMapping(value = "/index")
    public String index() {
        return "this is index";
    }
}
