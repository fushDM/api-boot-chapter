package org.minbox.chapter.apiboot.oauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
public class ApibootOauthUseRedisStorageApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApibootOauthUseRedisStorageApplication.class, args);
	}

}
