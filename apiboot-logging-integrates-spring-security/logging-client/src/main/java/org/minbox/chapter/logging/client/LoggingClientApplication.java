package org.minbox.chapter.logging.client;

import org.minbox.framework.logging.spring.context.annotation.client.EnableLoggingClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Logging Client
 *
 * @author 恒宇少年
 */
@SpringBootApplication
@EnableLoggingClient
public class LoggingClientApplication {
    /**
     * logger instance
     */
    static Logger logger = LoggerFactory.getLogger(LoggingClientApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(LoggingClientApplication.class, args);
        logger.info("{}服务启动成功.", "");
    }
}
