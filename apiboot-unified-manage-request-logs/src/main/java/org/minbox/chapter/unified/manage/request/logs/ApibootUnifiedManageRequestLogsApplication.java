package org.minbox.chapter.unified.manage.request.logs;

import org.minbox.framework.logging.spring.context.annotation.client.EnableLoggingClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 入口类
 *
 * @author 恒宇少年 - 于起宇
 */
@SpringBootApplication
@EnableLoggingClient
public class ApibootUnifiedManageRequestLogsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApibootUnifiedManageRequestLogsApplication.class, args);
    }

}
