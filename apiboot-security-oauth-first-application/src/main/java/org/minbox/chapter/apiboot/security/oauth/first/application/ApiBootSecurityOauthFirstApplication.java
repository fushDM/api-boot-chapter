package org.minbox.chapter.apiboot.security.oauth.first.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiBootSecurityOauthFirstApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiBootSecurityOauthFirstApplication.class, args);
    }

}
