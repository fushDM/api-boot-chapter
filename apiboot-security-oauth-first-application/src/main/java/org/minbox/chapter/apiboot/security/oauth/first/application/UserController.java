package org.minbox.chapter.apiboot.security.oauth.first.application;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * 登录用户信息
 *
 * @author 恒宇少年
 */
@RestController
@RequestMapping(value = "/api/user")
public class UserController {

    /**
     * 获取当前登录的用户信息
     * 通过Spring Security提供的注解{@link PreAuthorize}进行验证角色
     *
     * @param principal {@link Principal}
     * @return {@link Principal#getName()}
     */
    @GetMapping
    @PreAuthorize("hasRole('api')")
    public String info(Principal principal) {
        return principal.getName();
    }
}
