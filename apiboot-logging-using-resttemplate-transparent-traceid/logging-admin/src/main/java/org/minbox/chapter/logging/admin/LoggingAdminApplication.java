package org.minbox.chapter.logging.admin;

import org.minbox.framework.logging.spring.context.annotation.admin.EnableLoggingAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 恒宇少年
 */
@SpringBootApplication
@EnableLoggingAdmin
public class LoggingAdminApplication {
    /**
     * logger instance
     */
    static Logger logger = LoggerFactory.getLogger(LoggingAdminApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(LoggingAdminApplication.class, args);
        logger.info("{}服务启动成功.", "日志管理");
    }
}
